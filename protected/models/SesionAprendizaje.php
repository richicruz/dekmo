<?php

/**
 * This is the model class for table "sesion_aprendizaje".
 *
 * The followings are the available columns in table 'sesion_aprendizaje':
 * @property integer $id_sesion_aprendizaje
 * @property integer $id_unidad_aprendizaje
 * @property string $nombre_sesion_aprendizaje
 * @property string $qhd_in
 * @property string $qhe_in
 * @property string $recursos_in
 * @property integer $tiempo_minuto_in
 * @property string $qhd_proc
 * @property string $qhe_proc
 * @property string $recursos_proc
 * @property integer $tiempo_minuto_proc
 * @property string $qhd_res
 * @property string $qhe_res
 * @property string $recursos_res
 * @property integer $tiempo_minuto_res
 */
class SesionAprendizaje extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sesion_aprendizaje';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_unidad_aprendizaje, nombre_sesion_aprendizaje', 'required'),
			array('id_unidad_aprendizaje, tiempo_minuto_in, tiempo_minuto_proc, tiempo_minuto_res', 'numerical', 'integerOnly'=>true),
			array('nombre_sesion_aprendizaje', 'length', 'max'=>100),
			array('qhd_in, qhe_in, recursos_in, qhd_proc, qhe_proc, recursos_proc, qhd_res, qhe_res, recursos_res', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_sesion_aprendizaje, id_unidad_aprendizaje, nombre_sesion_aprendizaje, qhd_in, qhe_in, recursos_in, tiempo_minuto_in, qhd_proc, qhe_proc, recursos_proc, tiempo_minuto_proc, qhd_res, qhe_res, recursos_res, tiempo_minuto_res', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_sesion_aprendizaje' => 'Id Sesion Aprendizaje',
			'id_unidad_aprendizaje' => 'Id Unidad Aprendizaje',
			'nombre_sesion_aprendizaje' => 'Nombre Sesion Aprendizaje',
			'qhd_in' => 'Qhd In',
			'qhe_in' => 'Qhe In',
			'recursos_in' => 'Recursos In',
			'tiempo_minuto_in' => 'Tiempo Minuto In',
			'qhd_proc' => 'Qhd Proc',
			'qhe_proc' => 'Qhe Proc',
			'recursos_proc' => 'Recursos Proc',
			'tiempo_minuto_proc' => 'Tiempo Minuto Proc',
			'qhd_res' => 'Qhd Res',
			'qhe_res' => 'Qhe Res',
			'recursos_res' => 'Recursos Res',
			'tiempo_minuto_res' => 'Tiempo Minuto Res',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_sesion_aprendizaje',$this->id_sesion_aprendizaje);
		$criteria->compare('id_unidad_aprendizaje',$this->id_unidad_aprendizaje);
		$criteria->compare('nombre_sesion_aprendizaje',$this->nombre_sesion_aprendizaje,true);
		$criteria->compare('qhd_in',$this->qhd_in,true);
		$criteria->compare('qhe_in',$this->qhe_in,true);
		$criteria->compare('recursos_in',$this->recursos_in,true);
		$criteria->compare('tiempo_minuto_in',$this->tiempo_minuto_in);
		$criteria->compare('qhd_proc',$this->qhd_proc,true);
		$criteria->compare('qhe_proc',$this->qhe_proc,true);
		$criteria->compare('recursos_proc',$this->recursos_proc,true);
		$criteria->compare('tiempo_minuto_proc',$this->tiempo_minuto_proc);
		$criteria->compare('qhd_res',$this->qhd_res,true);
		$criteria->compare('qhe_res',$this->qhe_res,true);
		$criteria->compare('recursos_res',$this->recursos_res,true);
		$criteria->compare('tiempo_minuto_res',$this->tiempo_minuto_res);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SesionAprendizaje the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
