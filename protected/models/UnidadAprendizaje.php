<?php

/**
 * This is the model class for table "unidad_aprendizaje".
 *
 * The followings are the available columns in table 'unidad_aprendizaje':
 * @property integer $id_unidad_aprendizaje
 * @property string $codigo
 * @property string $nombre_unidad_aprendizaje
 * @property string $problema_contexto
 * @property string $criterio_desempenio
 * @property string $evidencia_aprendizaje
 * @property string $unidad_competencia
 * @property string $saber_hacer
 * @property string $saber_saber
 * @property string $saber_ser
 * @property string $receptivo
 * @property string $resolutivo
 * @property string $autonomo
 * @property string $estrategico
 * @property string $ponderacion_receptivo
 * @property string $ponderacion_resolutivo
 * @property string $ponderacion_autonomo
 * @property string $ponderacion_estrategico
 * @property string $autoeval_logros_evidencia
 * @property string $autoeval_acciones_mejorar
 * @property string $autoeval_nivel
 * @property string $autoeval_punteo
 * @property string $coeval_logros_evidencia
 * @property string $coeval_acciones_mejorar
 * @property string $coeval_nivel
 * @property string $coeval_punteo
 * @property string $heteroeval_logros_evidencia
 * @property string $heteroeval_acciones_mejorar
 * @property string $heteroeval_nivel
 * @property string $heteroeval_punteo
 * @property string $instrumento_eval
 * @property string $ponderacion
 */
class UnidadAprendizaje extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unidad_aprendizaje';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, nombre_unidad_aprendizaje', 'required'),
			array('codigo, ponderacion_receptivo, ponderacion_resolutivo, ponderacion_autonomo, ponderacion_estrategico, autoeval_nivel, autoeval_punteo, coeval_nivel, coeval_punteo, heteroeval_nivel, heteroeval_punteo, ponderacion', 'length', 'max'=>10),
			array('nombre_unidad_aprendizaje', 'length', 'max'=>25),
			array('problema_contexto, evidencia_aprendizaje, unidad_competencia, instrumento_eval', 'length', 'max'=>100),
			array('criterio_desempenio, saber_hacer, saber_saber, saber_ser, receptivo, resolutivo, autonomo, estrategico, autoeval_logros_evidencia, autoeval_acciones_mejorar, coeval_logros_evidencia, coeval_acciones_mejorar, heteroeval_logros_evidencia, heteroeval_acciones_mejorar', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_unidad_aprendizaje, codigo, nombre_unidad_aprendizaje, problema_contexto, criterio_desempenio, evidencia_aprendizaje, unidad_competencia, saber_hacer, saber_saber, saber_ser, receptivo, resolutivo, autonomo, estrategico, ponderacion_receptivo, ponderacion_resolutivo, ponderacion_autonomo, ponderacion_estrategico, autoeval_logros_evidencia, autoeval_acciones_mejorar, autoeval_nivel, autoeval_punteo, coeval_logros_evidencia, coeval_acciones_mejorar, coeval_nivel, coeval_punteo, heteroeval_logros_evidencia, heteroeval_acciones_mejorar, heteroeval_nivel, heteroeval_punteo, instrumento_eval, ponderacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_unidad_aprendizaje' => 'Id Unidad Aprendizaje',
			'codigo' => 'Codigo',
			'nombre_unidad_aprendizaje' => 'Nombre Unidad Aprendizaje',
			'problema_contexto' => 'Problema Contexto',
			'criterio_desempenio' => 'Criterio Desempenio',
			'evidencia_aprendizaje' => 'Evidencia Aprendizaje',
			'unidad_competencia' => 'Unidad Competencia',
			'saber_hacer' => 'Saber Hacer',
			'saber_saber' => 'Saber Saber',
			'saber_ser' => 'Saber Ser',
			'receptivo' => 'Receptivo',
			'resolutivo' => 'Resolutivo',
			'autonomo' => 'Autonomo',
			'estrategico' => 'Estrategico',
			'ponderacion_receptivo' => 'Ponderacion Receptivo',
			'ponderacion_resolutivo' => 'Ponderacion Resolutivo',
			'ponderacion_autonomo' => 'Ponderacion Autonomo',
			'ponderacion_estrategico' => 'Ponderacion Estrategico',
			'autoeval_logros_evidencia' => 'Autoeval Logros Evidencia',
			'autoeval_acciones_mejorar' => 'Autoeval Acciones Mejorar',
			'autoeval_nivel' => 'Autoeval Nivel',
			'autoeval_punteo' => 'Autoeval Punteo',
			'coeval_logros_evidencia' => 'Coeval Logros Evidencia',
			'coeval_acciones_mejorar' => 'Coeval Acciones Mejorar',
			'coeval_nivel' => 'Coeval Nivel',
			'coeval_punteo' => 'Coeval Punteo',
			'heteroeval_logros_evidencia' => 'Heteroeval Logros Evidencia',
			'heteroeval_acciones_mejorar' => 'Heteroeval Acciones Mejorar',
			'heteroeval_nivel' => 'Heteroeval Nivel',
			'heteroeval_punteo' => 'Heteroeval Punteo',
			'instrumento_eval' => 'Instrumento Eval',
			'ponderacion' => 'Ponderacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_unidad_aprendizaje',$this->id_unidad_aprendizaje);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('nombre_unidad_aprendizaje',$this->nombre_unidad_aprendizaje,true);
		$criteria->compare('problema_contexto',$this->problema_contexto,true);
		$criteria->compare('criterio_desempenio',$this->criterio_desempenio,true);
		$criteria->compare('evidencia_aprendizaje',$this->evidencia_aprendizaje,true);
		$criteria->compare('unidad_competencia',$this->unidad_competencia,true);
		$criteria->compare('saber_hacer',$this->saber_hacer,true);
		$criteria->compare('saber_saber',$this->saber_saber,true);
		$criteria->compare('saber_ser',$this->saber_ser,true);
		$criteria->compare('receptivo',$this->receptivo,true);
		$criteria->compare('resolutivo',$this->resolutivo,true);
		$criteria->compare('autonomo',$this->autonomo,true);
		$criteria->compare('estrategico',$this->estrategico,true);
		$criteria->compare('ponderacion_receptivo',$this->ponderacion_receptivo,true);
		$criteria->compare('ponderacion_resolutivo',$this->ponderacion_resolutivo,true);
		$criteria->compare('ponderacion_autonomo',$this->ponderacion_autonomo,true);
		$criteria->compare('ponderacion_estrategico',$this->ponderacion_estrategico,true);
		$criteria->compare('autoeval_logros_evidencia',$this->autoeval_logros_evidencia,true);
		$criteria->compare('autoeval_acciones_mejorar',$this->autoeval_acciones_mejorar,true);
		$criteria->compare('autoeval_nivel',$this->autoeval_nivel,true);
		$criteria->compare('autoeval_punteo',$this->autoeval_punteo,true);
		$criteria->compare('coeval_logros_evidencia',$this->coeval_logros_evidencia,true);
		$criteria->compare('coeval_acciones_mejorar',$this->coeval_acciones_mejorar,true);
		$criteria->compare('coeval_nivel',$this->coeval_nivel,true);
		$criteria->compare('coeval_punteo',$this->coeval_punteo,true);
		$criteria->compare('heteroeval_logros_evidencia',$this->heteroeval_logros_evidencia,true);
		$criteria->compare('heteroeval_acciones_mejorar',$this->heteroeval_acciones_mejorar,true);
		$criteria->compare('heteroeval_nivel',$this->heteroeval_nivel,true);
		$criteria->compare('heteroeval_punteo',$this->heteroeval_punteo,true);
		$criteria->compare('instrumento_eval',$this->instrumento_eval,true);
		$criteria->compare('ponderacion',$this->ponderacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UnidadAprendizaje the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
