<?php

/**
 * This is the model class for table "asignatura".
 *
 * The followings are the available columns in table 'asignatura':
 * @property string $codigo
 * @property integer $id_usuario
 * @property integer $rol
 * @property string $nombre
 * @property string $semestre
 * @property string $area
 * @property integer $creditos
 * @property string $tiempo_teoria
 * @property string $tiempo_practica
 * @property string $prerrequisitos
 * @property string $descripcion
 * @property string $competencia_perfil_egreso
 * @property string $competencia_area_curricular
 * @property string $metodologia
 * @property string $referencias_bibliograficas
 */
class Asignatura extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'asignatura';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, id_usuario, rol, nombre, semestre, area, creditos, tiempo_teoria, tiempo_practica, descripcion', 'required'),
			array('id_usuario, rol, creditos', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>10),
			array('nombre, competencia_perfil_egreso, competencia_area_curricular, metodologia', 'length', 'max'=>255),
			array('semestre', 'length', 'max'=>100),
			array('area, prerrequisitos', 'length', 'max'=>50),
			array('tiempo_teoria, tiempo_practica', 'length', 'max'=>15),
			array('descripcion', 'length', 'max'=>1024),
			array('referencias_bibliograficas', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('codigo, id_usuario, rol, nombre, semestre, area, creditos, tiempo_teoria, tiempo_practica, prerrequisitos, descripcion, competencia_perfil_egreso, competencia_area_curricular, metodologia, referencias_bibliograficas', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'codigo' => 'Codigo',
			'id_usuario' => 'Id Usuario',
			'rol' => 'Rol',
			'nombre' => 'Nombre',
			'semestre' => 'Semestre',
			'area' => 'Area',
			'creditos' => 'Creditos',
			'tiempo_teoria' => 'Tiempo Teoria',
			'tiempo_practica' => 'Tiempo Practica',
			'prerrequisitos' => 'Prerrequisitos',
			'descripcion' => 'Descripcion',
			'competencia_perfil_egreso' => 'Competencia Perfil Egreso',
			'competencia_area_curricular' => 'Competencia Area Curricular',
			'metodologia' => 'Metodologia',
			'referencias_bibliograficas' => 'Referencias Bibliograficas',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('id_usuario',$this->id_usuario);
		$criteria->compare('rol',$this->rol);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('semestre',$this->semestre,true);
		$criteria->compare('area',$this->area,true);
		$criteria->compare('creditos',$this->creditos);
		$criteria->compare('tiempo_teoria',$this->tiempo_teoria,true);
		$criteria->compare('tiempo_practica',$this->tiempo_practica,true);
		$criteria->compare('prerrequisitos',$this->prerrequisitos,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('competencia_perfil_egreso',$this->competencia_perfil_egreso,true);
		$criteria->compare('competencia_area_curricular',$this->competencia_area_curricular,true);
		$criteria->compare('metodologia',$this->metodologia,true);
		$criteria->compare('referencias_bibliograficas',$this->referencias_bibliograficas,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Asignatura the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
