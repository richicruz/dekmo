<?php
/* @var $this SesionAprendizajeController */
/* @var $data SesionAprendizaje */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_sesion_aprendizaje')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_sesion_aprendizaje), array('view', 'id'=>$data->id_sesion_aprendizaje)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_unidad_aprendizaje')); ?>:</b>
	<?php echo CHtml::encode($data->id_unidad_aprendizaje); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_sesion_aprendizaje')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_sesion_aprendizaje); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qhd_in')); ?>:</b>
	<?php echo CHtml::encode($data->qhd_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qhe_in')); ?>:</b>
	<?php echo CHtml::encode($data->qhe_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recursos_in')); ?>:</b>
	<?php echo CHtml::encode($data->recursos_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tiempo_minuto_in')); ?>:</b>
	<?php echo CHtml::encode($data->tiempo_minuto_in); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('qhd_proc')); ?>:</b>
	<?php echo CHtml::encode($data->qhd_proc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qhe_proc')); ?>:</b>
	<?php echo CHtml::encode($data->qhe_proc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recursos_proc')); ?>:</b>
	<?php echo CHtml::encode($data->recursos_proc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tiempo_minuto_proc')); ?>:</b>
	<?php echo CHtml::encode($data->tiempo_minuto_proc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qhd_res')); ?>:</b>
	<?php echo CHtml::encode($data->qhd_res); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qhe_res')); ?>:</b>
	<?php echo CHtml::encode($data->qhe_res); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recursos_res')); ?>:</b>
	<?php echo CHtml::encode($data->recursos_res); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tiempo_minuto_res')); ?>:</b>
	<?php echo CHtml::encode($data->tiempo_minuto_res); ?>
	<br />

	*/ ?>

</div>