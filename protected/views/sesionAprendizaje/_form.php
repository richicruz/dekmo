<?php
/* @var $this SesionAprendizajeController */
/* @var $model SesionAprendizaje */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sesion-aprendizaje-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_unidad_aprendizaje'); ?>
		<?php echo $form->textField($model,'id_unidad_aprendizaje'); ?>
		<?php echo $form->error($model,'id_unidad_aprendizaje'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre_sesion_aprendizaje'); ?>
		<?php echo $form->textField($model,'nombre_sesion_aprendizaje',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombre_sesion_aprendizaje'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qhd_in'); ?>
		<?php echo $form->textField($model,'qhd_in',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'qhd_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qhe_in'); ?>
		<?php echo $form->textField($model,'qhe_in',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'qhe_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recursos_in'); ?>
		<?php echo $form->textField($model,'recursos_in',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'recursos_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tiempo_minuto_in'); ?>
		<?php echo $form->textField($model,'tiempo_minuto_in'); ?>
		<?php echo $form->error($model,'tiempo_minuto_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qhd_proc'); ?>
		<?php echo $form->textField($model,'qhd_proc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'qhd_proc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qhe_proc'); ?>
		<?php echo $form->textField($model,'qhe_proc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'qhe_proc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recursos_proc'); ?>
		<?php echo $form->textField($model,'recursos_proc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'recursos_proc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tiempo_minuto_proc'); ?>
		<?php echo $form->textField($model,'tiempo_minuto_proc'); ?>
		<?php echo $form->error($model,'tiempo_minuto_proc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qhd_res'); ?>
		<?php echo $form->textField($model,'qhd_res',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'qhd_res'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qhe_res'); ?>
		<?php echo $form->textField($model,'qhe_res',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'qhe_res'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recursos_res'); ?>
		<?php echo $form->textField($model,'recursos_res',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'recursos_res'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tiempo_minuto_res'); ?>
		<?php echo $form->textField($model,'tiempo_minuto_res'); ?>
		<?php echo $form->error($model,'tiempo_minuto_res'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->