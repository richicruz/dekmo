<?php
/* @var $this SesionAprendizajeController */
/* @var $model SesionAprendizaje */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_sesion_aprendizaje'); ?>
		<?php echo $form->textField($model,'id_sesion_aprendizaje'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_unidad_aprendizaje'); ?>
		<?php echo $form->textField($model,'id_unidad_aprendizaje'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre_sesion_aprendizaje'); ?>
		<?php echo $form->textField($model,'nombre_sesion_aprendizaje',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qhd_in'); ?>
		<?php echo $form->textField($model,'qhd_in',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qhe_in'); ?>
		<?php echo $form->textField($model,'qhe_in',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'recursos_in'); ?>
		<?php echo $form->textField($model,'recursos_in',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tiempo_minuto_in'); ?>
		<?php echo $form->textField($model,'tiempo_minuto_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qhd_proc'); ?>
		<?php echo $form->textField($model,'qhd_proc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qhe_proc'); ?>
		<?php echo $form->textField($model,'qhe_proc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'recursos_proc'); ?>
		<?php echo $form->textField($model,'recursos_proc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tiempo_minuto_proc'); ?>
		<?php echo $form->textField($model,'tiempo_minuto_proc'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qhd_res'); ?>
		<?php echo $form->textField($model,'qhd_res',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qhe_res'); ?>
		<?php echo $form->textField($model,'qhe_res',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'recursos_res'); ?>
		<?php echo $form->textField($model,'recursos_res',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tiempo_minuto_res'); ?>
		<?php echo $form->textField($model,'tiempo_minuto_res'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->