<?php
/* @var $this SesionAprendizajeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sesion Aprendizajes',
);

$this->menu=array(
	array('label'=>'Create SesionAprendizaje', 'url'=>array('create')),
	array('label'=>'Manage SesionAprendizaje', 'url'=>array('admin')),
);
?>

<h1>Sesion Aprendizajes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
