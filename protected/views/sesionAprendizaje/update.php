<?php
/* @var $this SesionAprendizajeController */
/* @var $model SesionAprendizaje */

$this->breadcrumbs=array(
	'Sesion Aprendizajes'=>array('index'),
	$model->id_sesion_aprendizaje=>array('view','id'=>$model->id_sesion_aprendizaje),
	'Update',
);

$this->menu=array(
	array('label'=>'List SesionAprendizaje', 'url'=>array('index')),
	array('label'=>'Create SesionAprendizaje', 'url'=>array('create')),
	array('label'=>'View SesionAprendizaje', 'url'=>array('view', 'id'=>$model->id_sesion_aprendizaje)),
	array('label'=>'Manage SesionAprendizaje', 'url'=>array('admin')),
);
?>

<h1>Update SesionAprendizaje <?php echo $model->id_sesion_aprendizaje; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>