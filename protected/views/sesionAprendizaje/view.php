<?php
/* @var $this SesionAprendizajeController */
/* @var $model SesionAprendizaje */

$this->breadcrumbs=array(
	'Sesion Aprendizajes'=>array('index'),
	$model->id_sesion_aprendizaje,
);

$this->menu=array(
	array('label'=>'List SesionAprendizaje', 'url'=>array('index')),
	array('label'=>'Create SesionAprendizaje', 'url'=>array('create')),
	array('label'=>'Update SesionAprendizaje', 'url'=>array('update', 'id'=>$model->id_sesion_aprendizaje)),
	array('label'=>'Delete SesionAprendizaje', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_sesion_aprendizaje),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SesionAprendizaje', 'url'=>array('admin')),
);
?>

<h1>View SesionAprendizaje #<?php echo $model->id_sesion_aprendizaje; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_sesion_aprendizaje',
		'id_unidad_aprendizaje',
		'nombre_sesion_aprendizaje',
		'qhd_in',
		'qhe_in',
		'recursos_in',
		'tiempo_minuto_in',
		'qhd_proc',
		'qhe_proc',
		'recursos_proc',
		'tiempo_minuto_proc',
		'qhd_res',
		'qhe_res',
		'recursos_res',
		'tiempo_minuto_res',
	),
)); ?>
