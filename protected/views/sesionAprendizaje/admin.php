<?php
/* @var $this SesionAprendizajeController */
/* @var $model SesionAprendizaje */

$this->breadcrumbs=array(
	'Sesion Aprendizajes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SesionAprendizaje', 'url'=>array('index')),
	array('label'=>'Create SesionAprendizaje', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sesion-aprendizaje-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sesion Aprendizajes</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sesion-aprendizaje-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_sesion_aprendizaje',
		'id_unidad_aprendizaje',
		'nombre_sesion_aprendizaje',
		'qhd_in',
		'qhe_in',
		'recursos_in',
		/*
		'tiempo_minuto_in',
		'qhd_proc',
		'qhe_proc',
		'recursos_proc',
		'tiempo_minuto_proc',
		'qhd_res',
		'qhe_res',
		'recursos_res',
		'tiempo_minuto_res',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
