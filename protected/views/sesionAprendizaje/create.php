<?php
/* @var $this SesionAprendizajeController */
/* @var $model SesionAprendizaje */

$this->breadcrumbs=array(
	'Sesion Aprendizajes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SesionAprendizaje', 'url'=>array('index')),
	array('label'=>'Manage SesionAprendizaje', 'url'=>array('admin')),
);
?>

<h1>Create SesionAprendizaje</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>