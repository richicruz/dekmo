<?php
/* @var $this AsignaturaController */
/* @var $model Asignatura */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'asignatura-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'codigo'); ?>
		<?php echo $form->textField($model,'codigo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_usuario'); ?>
		<?php echo $form->textField($model,'id_usuario'); ?>
		<?php echo $form->error($model,'id_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rol'); ?>
		<?php echo $form->textField($model,'rol'); ?>
		<?php echo $form->error($model,'rol'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'semestre'); ?>
		<?php echo $form->textField($model,'semestre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'semestre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'area'); ?>
		<?php echo $form->textField($model,'area',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'area'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'creditos'); ?>
		<?php echo $form->textField($model,'creditos'); ?>
		<?php echo $form->error($model,'creditos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tiempo_teoria'); ?>
		<?php echo $form->textField($model,'tiempo_teoria',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'tiempo_teoria'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tiempo_practica'); ?>
		<?php echo $form->textField($model,'tiempo_practica',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'tiempo_practica'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prerrequisitos'); ?>
		<?php echo $form->textField($model,'prerrequisitos',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'prerrequisitos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'competencia_perfil_egreso'); ?>
		<?php echo $form->textField($model,'competencia_perfil_egreso',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'competencia_perfil_egreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'competencia_area_curricular'); ?>
		<?php echo $form->textField($model,'competencia_area_curricular',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'competencia_area_curricular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'metodologia'); ?>
		<?php echo $form->textField($model,'metodologia',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'metodologia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'referencias_bibliograficas'); ?>
		<?php echo $form->textArea($model,'referencias_bibliograficas',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'referencias_bibliograficas'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->