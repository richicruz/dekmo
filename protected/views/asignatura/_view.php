<?php
/* @var $this AsignaturaController */
/* @var $data Asignatura */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->codigo), array('view', 'id'=>$data->codigo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->id_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rol')); ?>:</b>
	<?php echo CHtml::encode($data->rol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('semestre')); ?>:</b>
	<?php echo CHtml::encode($data->semestre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area')); ?>:</b>
	<?php echo CHtml::encode($data->area); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creditos')); ?>:</b>
	<?php echo CHtml::encode($data->creditos); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tiempo_teoria')); ?>:</b>
	<?php echo CHtml::encode($data->tiempo_teoria); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tiempo_practica')); ?>:</b>
	<?php echo CHtml::encode($data->tiempo_practica); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prerrequisitos')); ?>:</b>
	<?php echo CHtml::encode($data->prerrequisitos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('competencia_perfil_egreso')); ?>:</b>
	<?php echo CHtml::encode($data->competencia_perfil_egreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('competencia_area_curricular')); ?>:</b>
	<?php echo CHtml::encode($data->competencia_area_curricular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('metodologia')); ?>:</b>
	<?php echo CHtml::encode($data->metodologia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('referencias_bibliograficas')); ?>:</b>
	<?php echo CHtml::encode($data->referencias_bibliograficas); ?>
	<br />

	*/ ?>

</div>