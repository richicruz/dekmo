<?php
/* @var $this AsignaturaController */
/* @var $model Asignatura */

$this->breadcrumbs=array(
	'Asignaturas'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'List Asignatura', 'url'=>array('index')),
	array('label'=>'Create Asignatura', 'url'=>array('create')),
	array('label'=>'Update Asignatura', 'url'=>array('update', 'id'=>$model->codigo)),
	array('label'=>'Delete Asignatura', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->codigo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Asignatura', 'url'=>array('admin')),
);
?>

<h1>View Asignatura #<?php echo $model->codigo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'codigo',
		'id_usuario',
		'rol',
		'nombre',
		'semestre',
		'area',
		'creditos',
		'tiempo_teoria',
		'tiempo_practica',
		'prerrequisitos',
		'descripcion',
		'competencia_perfil_egreso',
		'competencia_area_curricular',
		'metodologia',
		'referencias_bibliograficas',
	),
)); ?>
