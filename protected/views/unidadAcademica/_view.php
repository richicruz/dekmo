<?php
/* @var $this UnidadAcademicaController */
/* @var $data UnidadAcademica */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_unidad_academica')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_unidad_academica), array('view', 'id'=>$data->id_unidad_academica)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />


</div>