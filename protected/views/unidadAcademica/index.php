<?php
/* @var $this UnidadAcademicaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Unidad Academicas',
);

$this->menu=array(
	array('label'=>'Create UnidadAcademica', 'url'=>array('create')),
	array('label'=>'Manage UnidadAcademica', 'url'=>array('admin')),
);
?>

<h1>Unidad Academicas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
