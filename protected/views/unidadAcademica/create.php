<?php
/* @var $this UnidadAcademicaController */
/* @var $model UnidadAcademica */

$this->breadcrumbs=array(
	'Unidad Academicas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UnidadAcademica', 'url'=>array('index')),
	array('label'=>'Manage UnidadAcademica', 'url'=>array('admin')),
);
?>

<h1>Create UnidadAcademica</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>