<?php
/* @var $this UnidadAcademicaController */
/* @var $model UnidadAcademica */

$this->breadcrumbs=array(
	'Unidad Academicas'=>array('index'),
	$model->id_unidad_academica=>array('view','id'=>$model->id_unidad_academica),
	'Update',
);

$this->menu=array(
	array('label'=>'List UnidadAcademica', 'url'=>array('index')),
	array('label'=>'Create UnidadAcademica', 'url'=>array('create')),
	array('label'=>'View UnidadAcademica', 'url'=>array('view', 'id'=>$model->id_unidad_academica)),
	array('label'=>'Manage UnidadAcademica', 'url'=>array('admin')),
);
?>

<h1>Update UnidadAcademica <?php echo $model->id_unidad_academica; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>