<?php
/* @var $this UnidadAcademicaController */
/* @var $model UnidadAcademica */

$this->breadcrumbs=array(
	'Unidad Academicas'=>array('index'),
	$model->id_unidad_academica,
);

$this->menu=array(
	array('label'=>'List UnidadAcademica', 'url'=>array('index')),
	array('label'=>'Create UnidadAcademica', 'url'=>array('create')),
	array('label'=>'Update UnidadAcademica', 'url'=>array('update', 'id'=>$model->id_unidad_academica)),
	array('label'=>'Delete UnidadAcademica', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_unidad_academica),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UnidadAcademica', 'url'=>array('admin')),
);
?>

<h1>View UnidadAcademica #<?php echo $model->id_unidad_academica; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_unidad_academica',
		'nombre',
	),
)); ?>
