<?php
/* @var $this UnidadAprendizajeController */
/* @var $model UnidadAprendizaje */

$this->breadcrumbs=array(
	'Unidad Aprendizajes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List UnidadAprendizaje', 'url'=>array('index')),
	array('label'=>'Create UnidadAprendizaje', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#unidad-aprendizaje-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Unidad Aprendizajes</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'unidad-aprendizaje-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_unidad_aprendizaje',
		'codigo',
		'nombre_unidad_aprendizaje',
		'problema_contexto',
		'criterio_desempenio',
		'evidencia_aprendizaje',
		/*
		'unidad_competencia',
		'saber_hacer',
		'saber_saber',
		'saber_ser',
		'receptivo',
		'resolutivo',
		'autonomo',
		'estrategico',
		'ponderacion_receptivo',
		'ponderacion_resolutivo',
		'ponderacion_autonomo',
		'ponderacion_estrategico',
		'autoeval_logros_evidencia',
		'autoeval_acciones_mejorar',
		'autoeval_nivel',
		'autoeval_punteo',
		'coeval_logros_evidencia',
		'coeval_acciones_mejorar',
		'coeval_nivel',
		'coeval_punteo',
		'heteroeval_logros_evidencia',
		'heteroeval_acciones_mejorar',
		'heteroeval_nivel',
		'heteroeval_punteo',
		'instrumento_eval',
		'ponderacion',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
