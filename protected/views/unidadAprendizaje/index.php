<?php
/* @var $this UnidadAprendizajeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Unidad Aprendizajes',
);

$this->menu=array(
	array('label'=>'Create UnidadAprendizaje', 'url'=>array('create')),
	array('label'=>'Manage UnidadAprendizaje', 'url'=>array('admin')),
);
?>

<h1>Unidad Aprendizajes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
