<?php
/* @var $this UnidadAprendizajeController */
/* @var $model UnidadAprendizaje */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'unidad-aprendizaje-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'codigo'); ?>
		<?php echo $form->textField($model,'codigo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre_unidad_aprendizaje'); ?>
		<?php echo $form->textField($model,'nombre_unidad_aprendizaje',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'nombre_unidad_aprendizaje'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'problema_contexto'); ?>
		<?php echo $form->textField($model,'problema_contexto',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'problema_contexto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'criterio_desempenio'); ?>
		<?php echo $form->textField($model,'criterio_desempenio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'criterio_desempenio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'evidencia_aprendizaje'); ?>
		<?php echo $form->textField($model,'evidencia_aprendizaje',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'evidencia_aprendizaje'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unidad_competencia'); ?>
		<?php echo $form->textField($model,'unidad_competencia',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'unidad_competencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saber_hacer'); ?>
		<?php echo $form->textField($model,'saber_hacer',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'saber_hacer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saber_saber'); ?>
		<?php echo $form->textField($model,'saber_saber',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'saber_saber'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'saber_ser'); ?>
		<?php echo $form->textField($model,'saber_ser',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'saber_ser'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'receptivo'); ?>
		<?php echo $form->textField($model,'receptivo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'receptivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'resolutivo'); ?>
		<?php echo $form->textField($model,'resolutivo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'resolutivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'autonomo'); ?>
		<?php echo $form->textField($model,'autonomo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'autonomo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estrategico'); ?>
		<?php echo $form->textField($model,'estrategico',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'estrategico'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ponderacion_receptivo'); ?>
		<?php echo $form->textField($model,'ponderacion_receptivo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ponderacion_receptivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ponderacion_resolutivo'); ?>
		<?php echo $form->textField($model,'ponderacion_resolutivo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ponderacion_resolutivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ponderacion_autonomo'); ?>
		<?php echo $form->textField($model,'ponderacion_autonomo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ponderacion_autonomo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ponderacion_estrategico'); ?>
		<?php echo $form->textField($model,'ponderacion_estrategico',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ponderacion_estrategico'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'autoeval_logros_evidencia'); ?>
		<?php echo $form->textField($model,'autoeval_logros_evidencia',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'autoeval_logros_evidencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'autoeval_acciones_mejorar'); ?>
		<?php echo $form->textField($model,'autoeval_acciones_mejorar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'autoeval_acciones_mejorar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'autoeval_nivel'); ?>
		<?php echo $form->textField($model,'autoeval_nivel',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'autoeval_nivel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'autoeval_punteo'); ?>
		<?php echo $form->textField($model,'autoeval_punteo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'autoeval_punteo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coeval_logros_evidencia'); ?>
		<?php echo $form->textField($model,'coeval_logros_evidencia',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'coeval_logros_evidencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coeval_acciones_mejorar'); ?>
		<?php echo $form->textField($model,'coeval_acciones_mejorar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'coeval_acciones_mejorar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coeval_nivel'); ?>
		<?php echo $form->textField($model,'coeval_nivel',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'coeval_nivel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coeval_punteo'); ?>
		<?php echo $form->textField($model,'coeval_punteo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'coeval_punteo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'heteroeval_logros_evidencia'); ?>
		<?php echo $form->textField($model,'heteroeval_logros_evidencia',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'heteroeval_logros_evidencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'heteroeval_acciones_mejorar'); ?>
		<?php echo $form->textField($model,'heteroeval_acciones_mejorar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'heteroeval_acciones_mejorar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'heteroeval_nivel'); ?>
		<?php echo $form->textField($model,'heteroeval_nivel',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'heteroeval_nivel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'heteroeval_punteo'); ?>
		<?php echo $form->textField($model,'heteroeval_punteo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'heteroeval_punteo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'instrumento_eval'); ?>
		<?php echo $form->textField($model,'instrumento_eval',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'instrumento_eval'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ponderacion'); ?>
		<?php echo $form->textField($model,'ponderacion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'ponderacion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->