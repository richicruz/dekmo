<?php
/* @var $this UnidadAprendizajeController */
/* @var $model UnidadAprendizaje */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_unidad_aprendizaje'); ?>
		<?php echo $form->textField($model,'id_unidad_aprendizaje'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'codigo'); ?>
		<?php echo $form->textField($model,'codigo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre_unidad_aprendizaje'); ?>
		<?php echo $form->textField($model,'nombre_unidad_aprendizaje',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'problema_contexto'); ?>
		<?php echo $form->textField($model,'problema_contexto',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'criterio_desempenio'); ?>
		<?php echo $form->textField($model,'criterio_desempenio',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'evidencia_aprendizaje'); ?>
		<?php echo $form->textField($model,'evidencia_aprendizaje',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unidad_competencia'); ?>
		<?php echo $form->textField($model,'unidad_competencia',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'saber_hacer'); ?>
		<?php echo $form->textField($model,'saber_hacer',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'saber_saber'); ?>
		<?php echo $form->textField($model,'saber_saber',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'saber_ser'); ?>
		<?php echo $form->textField($model,'saber_ser',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'receptivo'); ?>
		<?php echo $form->textField($model,'receptivo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'resolutivo'); ?>
		<?php echo $form->textField($model,'resolutivo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'autonomo'); ?>
		<?php echo $form->textField($model,'autonomo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estrategico'); ?>
		<?php echo $form->textField($model,'estrategico',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ponderacion_receptivo'); ?>
		<?php echo $form->textField($model,'ponderacion_receptivo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ponderacion_resolutivo'); ?>
		<?php echo $form->textField($model,'ponderacion_resolutivo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ponderacion_autonomo'); ?>
		<?php echo $form->textField($model,'ponderacion_autonomo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ponderacion_estrategico'); ?>
		<?php echo $form->textField($model,'ponderacion_estrategico',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'autoeval_logros_evidencia'); ?>
		<?php echo $form->textField($model,'autoeval_logros_evidencia',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'autoeval_acciones_mejorar'); ?>
		<?php echo $form->textField($model,'autoeval_acciones_mejorar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'autoeval_nivel'); ?>
		<?php echo $form->textField($model,'autoeval_nivel',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'autoeval_punteo'); ?>
		<?php echo $form->textField($model,'autoeval_punteo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coeval_logros_evidencia'); ?>
		<?php echo $form->textField($model,'coeval_logros_evidencia',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coeval_acciones_mejorar'); ?>
		<?php echo $form->textField($model,'coeval_acciones_mejorar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coeval_nivel'); ?>
		<?php echo $form->textField($model,'coeval_nivel',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coeval_punteo'); ?>
		<?php echo $form->textField($model,'coeval_punteo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'heteroeval_logros_evidencia'); ?>
		<?php echo $form->textField($model,'heteroeval_logros_evidencia',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'heteroeval_acciones_mejorar'); ?>
		<?php echo $form->textField($model,'heteroeval_acciones_mejorar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'heteroeval_nivel'); ?>
		<?php echo $form->textField($model,'heteroeval_nivel',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'heteroeval_punteo'); ?>
		<?php echo $form->textField($model,'heteroeval_punteo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'instrumento_eval'); ?>
		<?php echo $form->textField($model,'instrumento_eval',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ponderacion'); ?>
		<?php echo $form->textField($model,'ponderacion',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->