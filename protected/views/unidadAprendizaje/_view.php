<?php
/* @var $this UnidadAprendizajeController */
/* @var $data UnidadAprendizaje */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_unidad_aprendizaje')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_unidad_aprendizaje), array('view', 'id'=>$data->id_unidad_aprendizaje)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::encode($data->codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_unidad_aprendizaje')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_unidad_aprendizaje); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('problema_contexto')); ?>:</b>
	<?php echo CHtml::encode($data->problema_contexto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('criterio_desempenio')); ?>:</b>
	<?php echo CHtml::encode($data->criterio_desempenio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('evidencia_aprendizaje')); ?>:</b>
	<?php echo CHtml::encode($data->evidencia_aprendizaje); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unidad_competencia')); ?>:</b>
	<?php echo CHtml::encode($data->unidad_competencia); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('saber_hacer')); ?>:</b>
	<?php echo CHtml::encode($data->saber_hacer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saber_saber')); ?>:</b>
	<?php echo CHtml::encode($data->saber_saber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saber_ser')); ?>:</b>
	<?php echo CHtml::encode($data->saber_ser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('receptivo')); ?>:</b>
	<?php echo CHtml::encode($data->receptivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('resolutivo')); ?>:</b>
	<?php echo CHtml::encode($data->resolutivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('autonomo')); ?>:</b>
	<?php echo CHtml::encode($data->autonomo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estrategico')); ?>:</b>
	<?php echo CHtml::encode($data->estrategico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ponderacion_receptivo')); ?>:</b>
	<?php echo CHtml::encode($data->ponderacion_receptivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ponderacion_resolutivo')); ?>:</b>
	<?php echo CHtml::encode($data->ponderacion_resolutivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ponderacion_autonomo')); ?>:</b>
	<?php echo CHtml::encode($data->ponderacion_autonomo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ponderacion_estrategico')); ?>:</b>
	<?php echo CHtml::encode($data->ponderacion_estrategico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('autoeval_logros_evidencia')); ?>:</b>
	<?php echo CHtml::encode($data->autoeval_logros_evidencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('autoeval_acciones_mejorar')); ?>:</b>
	<?php echo CHtml::encode($data->autoeval_acciones_mejorar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('autoeval_nivel')); ?>:</b>
	<?php echo CHtml::encode($data->autoeval_nivel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('autoeval_punteo')); ?>:</b>
	<?php echo CHtml::encode($data->autoeval_punteo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coeval_logros_evidencia')); ?>:</b>
	<?php echo CHtml::encode($data->coeval_logros_evidencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coeval_acciones_mejorar')); ?>:</b>
	<?php echo CHtml::encode($data->coeval_acciones_mejorar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coeval_nivel')); ?>:</b>
	<?php echo CHtml::encode($data->coeval_nivel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coeval_punteo')); ?>:</b>
	<?php echo CHtml::encode($data->coeval_punteo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('heteroeval_logros_evidencia')); ?>:</b>
	<?php echo CHtml::encode($data->heteroeval_logros_evidencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('heteroeval_acciones_mejorar')); ?>:</b>
	<?php echo CHtml::encode($data->heteroeval_acciones_mejorar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('heteroeval_nivel')); ?>:</b>
	<?php echo CHtml::encode($data->heteroeval_nivel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('heteroeval_punteo')); ?>:</b>
	<?php echo CHtml::encode($data->heteroeval_punteo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instrumento_eval')); ?>:</b>
	<?php echo CHtml::encode($data->instrumento_eval); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ponderacion')); ?>:</b>
	<?php echo CHtml::encode($data->ponderacion); ?>
	<br />

	*/ ?>

</div>