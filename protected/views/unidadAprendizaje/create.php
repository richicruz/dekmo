<?php
/* @var $this UnidadAprendizajeController */
/* @var $model UnidadAprendizaje */

$this->breadcrumbs=array(
	'Unidad Aprendizajes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UnidadAprendizaje', 'url'=>array('index')),
	array('label'=>'Manage UnidadAprendizaje', 'url'=>array('admin')),
);
?>

<h1>Create UnidadAprendizaje</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>