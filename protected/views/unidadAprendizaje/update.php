<?php
/* @var $this UnidadAprendizajeController */
/* @var $model UnidadAprendizaje */

$this->breadcrumbs=array(
	'Unidad Aprendizajes'=>array('index'),
	$model->id_unidad_aprendizaje=>array('view','id'=>$model->id_unidad_aprendizaje),
	'Update',
);

$this->menu=array(
	array('label'=>'List UnidadAprendizaje', 'url'=>array('index')),
	array('label'=>'Create UnidadAprendizaje', 'url'=>array('create')),
	array('label'=>'View UnidadAprendizaje', 'url'=>array('view', 'id'=>$model->id_unidad_aprendizaje)),
	array('label'=>'Manage UnidadAprendizaje', 'url'=>array('admin')),
);
?>

<h1>Update UnidadAprendizaje <?php echo $model->id_unidad_aprendizaje; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>