<?php
/* @var $this UnidadAprendizajeController */
/* @var $model UnidadAprendizaje */

$this->breadcrumbs=array(
	'Unidad Aprendizajes'=>array('index'),
	$model->id_unidad_aprendizaje,
);

$this->menu=array(
	array('label'=>'List UnidadAprendizaje', 'url'=>array('index')),
	array('label'=>'Create UnidadAprendizaje', 'url'=>array('create')),
	array('label'=>'Update UnidadAprendizaje', 'url'=>array('update', 'id'=>$model->id_unidad_aprendizaje)),
	array('label'=>'Delete UnidadAprendizaje', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_unidad_aprendizaje),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UnidadAprendizaje', 'url'=>array('admin')),
);
?>

<h1>View UnidadAprendizaje #<?php echo $model->id_unidad_aprendizaje; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_unidad_aprendizaje',
		'codigo',
		'nombre_unidad_aprendizaje',
		'problema_contexto',
		'criterio_desempenio',
		'evidencia_aprendizaje',
		'unidad_competencia',
		'saber_hacer',
		'saber_saber',
		'saber_ser',
		'receptivo',
		'resolutivo',
		'autonomo',
		'estrategico',
		'ponderacion_receptivo',
		'ponderacion_resolutivo',
		'ponderacion_autonomo',
		'ponderacion_estrategico',
		'autoeval_logros_evidencia',
		'autoeval_acciones_mejorar',
		'autoeval_nivel',
		'autoeval_punteo',
		'coeval_logros_evidencia',
		'coeval_acciones_mejorar',
		'coeval_nivel',
		'coeval_punteo',
		'heteroeval_logros_evidencia',
		'heteroeval_acciones_mejorar',
		'heteroeval_nivel',
		'heteroeval_punteo',
		'instrumento_eval',
		'ponderacion',
	),
)); ?>
